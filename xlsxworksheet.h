// xlsxworksheet.h

#ifndef XLSXWORKSHEET_H
#define XLSXWORKSHEET_H

#include <QDateTime>
#include <QImage>
#include <QIODevice>
#include <QMap>
#include <QObject>
#include <QPointF>
#include <QSharedPointer>
#include <QStringList>
#include <QtGlobal>
#include <QUrl>
#include <QVariant>

#include "xlsxabstractsheet.h"
#include "xlsxcell.h"
#include "xlsxcelllocation.h"
#include "xlsxcellrange.h"
#include "xlsxcellreference.h"

class WorksheetTest;

QT_BEGIN_NAMESPACE_XLSX

class DocumentPrivate;
class Workbook;
class Format;
class Drawing;
class DataValidation;
class ConditionalFormatting;
class CellRange;
class RichString;
class Relationships;
class Chart;

class WorksheetPrivate;
class Worksheet: public AbstractSheet
{
	Q_DECLARE_PRIVATE(Worksheet)

private:
	friend class DocumentPrivate;
	friend class Workbook;
	friend class ::WorksheetTest;
	Worksheet(QString const &sheetName, int sheetId, Workbook *book, CreateFlag flag);
	Worksheet *copy(QString const &distName, int distId) const;

public:
	~Worksheet();

public:
	bool write(CellReference const &row_column, QVariant const &value, Format const &format = Format());
	bool write(int row, int column, QVariant const &value, Format const &format = Format());

	QVariant read(CellReference const &row_column) const;
	QVariant read(int row, int column) const;

	bool writeString(CellReference const &row_column, QString const &value, Format const &format = Format());
	bool writeString(int row, int column, QString const &value, Format const &format = Format());
	bool writeString(CellReference const &row_column, RichString const &value, Format const &format = Format());
	bool writeString(int row, int column, RichString const &value, Format const &format = Format());

	bool writeInlineString(CellReference const &row_column, QString const &value, Format const &format = Format());
	bool writeInlineString(int row, int column, QString const &value, Format const &format = Format());

	bool writeNumeric(CellReference const &row_column, double value, Format const &format = Format());
	bool writeNumeric(int row, int column, double value, Format const &format = Format());

	bool writeFormula(CellReference const &row_column, CellFormula const &formula, Format const &format = Format(), double result = 0);
	bool writeFormula(int row, int column, CellFormula const &formula, Format const &format = Format(), double result = 0);

	bool writeBlank(CellReference const &row_column, Format const &format = Format());
	bool writeBlank(int row, int column, Format const &format = Format());

	bool writeBool(CellReference const &row_column, bool value, Format const &format = Format());
	bool writeBool(int row, int column, bool value, Format const &format = Format());

	bool writeDateTime(CellReference const &row_column, const QDateTime&dt, Format const &format = Format());
	bool writeDateTime(int row, int column, const QDateTime&dt, Format const &format = Format());

	// dev67
	bool writeDate(CellReference const &row_column, const QDate&dt, Format const &format = Format());
	bool writeDate(int row, int column, const QDate&dt, Format const &format = Format());

	bool writeTime(CellReference const &row_column, const QTime&t, Format const &format = Format());
	bool writeTime(int row, int column, const QTime&t, Format const &format = Format());

	bool writeHyperlink(CellReference const &row_column, QUrl const &url, Format const &format = Format(), QString const &display = QString(), QString const &tip = QString());
	bool writeHyperlink(int row, int column, QUrl const &url, Format const &format = Format(), QString const &display = QString(), QString const &tip = QString());

	bool addDataValidation(DataValidation const &validation);
	bool addConditionalFormatting(ConditionalFormatting const &cf);

	Cell *cellAt(CellReference const &row_column) const;
	Cell *cellAt(int row, int column) const;

	bool             mergeCells(CellRange const &range, Format const &format = Format());
	bool             unmergeCells(CellRange const &range);
	QList<CellRange> mergedCells() const;

	bool setColumnWidth(const CellRange&range, double width);
	bool setColumnFormat(const CellRange&range, Format const &format);
	bool setColumnHidden(const CellRange&range, bool hidden);
	bool setColumnWidth(int colFirst, int colLast, double width);
	bool setColumnFormat(int colFirst, int colLast, Format const &format);
	bool setColumnHidden(int colFirst, int colLast, bool hidden);

	double columnWidth(int column);
	Format columnFormat(int column);
	bool   isColumnHidden(int column);

	bool setRowHeight(int rowFirst, int rowLast, double height);
	bool setRowFormat(int rowFirst, int rowLast, Format const &format);
	bool setRowHidden(int rowFirst, int rowLast, bool hidden);

	double rowHeight(int row);
	Format rowFormat(int row);
	bool   isRowHidden(int row);

	bool      groupRows(int rowFirst, int rowLast, bool collapsed = true);
	bool      groupColumns(int colFirst, int colLast, bool collapsed = true);
	bool      groupColumns(CellRange const &range, bool collapsed = true);
	CellRange dimension() const;

	bool isWindowProtected() const;
	void setWindowProtected(bool protect);
	bool isFormulasVisible() const;
	void setFormulasVisible(bool visible);
	bool isGridLinesVisible() const;
	void setGridLinesVisible(bool visible);
	bool isRowColumnHeadersVisible() const;
	void setRowColumnHeadersVisible(bool visible);
	bool isZerosVisible() const;
	void setZerosVisible(bool visible);
	bool isRightToLeft() const;
	void setRightToLeft(bool enable);
	bool isSelected() const;
	void setSelected(bool select);
	bool isRulerVisible() const;
	void setRulerVisible(bool visible);
	bool isOutlineSymbolsVisible() const;
	void setOutlineSymbolsVisible(bool visible);
	bool isWhiteSpaceVisible() const;
	void setWhiteSpaceVisible(bool visible);
	bool setStartPage(int spagen); //add by liufeijin20181028

	QVector<CellLocation> getFullCells(int &maxRow, int &maxCol);

private:
	void saveToXmlFile(QIODevice *device) const;
	bool loadFromXmlFile(QIODevice *device);
};

QT_END_NAMESPACE_XLSX
#endif // XLSXWORKSHEET_H
