// xlsxdocument.h

#ifndef QXLSX_XLSXDOCUMENT_H
#define QXLSX_XLSXDOCUMENT_H

#include <QImage>
#include <QIODevice>
#include <QObject>
#include <QtGlobal>
#include <QVariant>

#include "xlsxformat.h"
#include "xlsxglobal.h"
#include "xlsxworksheet.h"

QT_BEGIN_NAMESPACE_XLSX

class Workbook;
class Cell;
class CellRange;
class DataValidation;
class ConditionalFormatting;
class Chart;
class CellReference;
class DocumentPrivate;

class Document: public QObject
{
	Q_OBJECT
	Q_DECLARE_PRIVATE(Document) // D-Pointer. Qt classes have a Q_DECLARE_PRIVATE
	                            // macro in the public class. The macro reads: qglobal.h

public:
	explicit Document(QObject *parent = NULL);
	Document(const QString&xlsxName, QObject*parent = NULL);
	Document(QIODevice*device, QObject*parent = NULL);
	~Document();

	bool write(CellReference const &cell, QVariant const &value, Format const &format = Format());
	bool write(int row, int col, QVariant const &value, Format const &format = Format());

	QVariant read(CellReference const &cell) const;
	QVariant read(int row, int col) const;
	QVariant readValue(int row, int column) const;

	bool mergeCells(CellRange const &range, Format const &format = Format());
	bool unmergeCells(CellRange const &range);

	bool setColumnWidth(CellRange const &range, double width);
	bool setColumnFormat(CellRange const &range, Format const &format);
	bool setColumnHidden(CellRange const &range, bool hidden);
	bool setColumnWidth(int column, double width);
	bool setColumnFormat(int column, Format const &format);
	bool setColumnHidden(int column, bool hidden);
	bool setColumnWidth(int colFirst, int colLast, double width);
	bool setColumnFormat(int colFirst, int colLast, Format const &format);
	bool setColumnHidden(int colFirst, int colLast, bool hidden);

	double columnWidth(int column);
	Format columnFormat(int column);
	bool   isColumnHidden(int column);

	bool setRowHeight(int row, double height);
	bool setRowFormat(int row, Format const &format);
	bool setRowHidden(int row, bool hidden);
	bool setRowHeight(int rowFirst, int rowLast, double height);
	bool setRowFormat(int rowFirst, int rowLast, Format const &format);
	bool setRowHidden(int rowFirst, int rowLast, bool hidden);

	double rowHeight(int row);
	Format rowFormat(int row);
	bool   isRowHidden(int row);

	bool groupRows(int rowFirst, int rowLast, bool collapsed = true);
	bool groupColumns(int colFirst, int colLast, bool collapsed = true);

	bool addDataValidation(DataValidation const &validation);
	bool addConditionalFormatting(ConditionalFormatting const &cf);

	Cell *cellAt(CellReference const &cell) const;
	Cell *cellAt(int row, int col) const;

	bool defineName(QString const &name, QString const &formula,
					QString const &comment = QString(), QString const &scope = QString());

	CellRange dimension() const;

	QString     documentProperty(QString const &name) const;
	void        setDocumentProperty(QString const &name, QString const &property);
	QStringList documentPropertyNames() const;

	QStringList sheetNames() const;
	bool        addSheet(QString const &name = QString(),
	                     AbstractSheet::SheetType type = AbstractSheet::ST_WorkSheet);
	bool insertSheet(int index, QString const &name = QString(),
	                 AbstractSheet::SheetType type = AbstractSheet::ST_WorkSheet);
	bool selectSheet(QString const &name);
	bool renameSheet(QString const &oldName, QString const &newName);
	bool copySheet(QString const &srcName, QString const &distName = QString());
	bool moveSheet(QString const &srcName, int distIndex);
	bool deleteSheet(QString const &name);

	Workbook      *workbook() const;
	AbstractSheet *sheet(QString const &sheetName) const;
	AbstractSheet *currentSheet() const;
	Worksheet     *currentWorksheet() const;

	bool save() const;
	bool saveAs(QString const &xlsXname) const;
	bool saveAs(QIODevice *device) const;

	// copy style from one xlsx file to other
	static bool copyStyle(QString const &from, QString const &to);

	bool isLoadPackage() const;
	bool load() const; // equals to isLoadPackage()

	bool changeimage(int filenoinmidea, QString newfile); // add by liufeijin20181025

	bool autosizeColumnWidth(CellRange const &range);
	bool autosizeColumnWidth(int column);
	bool autosizeColumnWidth(int colFirst, int colLast);
	bool autosizeColumnWidth(void);

private:
	QMap<int, int> getMaximalColumnWidth(int firstRow = 1, int lastRow = INT_MAX);

private:
	Q_DISABLE_COPY(Document) // Disables the use of copy constructors and
	                         // assignment operators for the given Class.
	DocumentPrivate*const d_ptr;
};

QT_END_NAMESPACE_XLSX

#endif // QXLSX_XLSXDOCUMENT_H
